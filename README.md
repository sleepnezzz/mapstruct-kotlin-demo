# MapStruct with Kotlin

example want to map between Person Model and Person... DTO

## Gradle Setup

At `build.gradle.kts` Add `plugins` and `dependencies`

```groov
plugins {
    kotlin("kapt") version "1.7.10"
}

dependencies {
    implementation("org.mapstruct:mapstruct:1.5.2.Final")
    kapt("org.mapstruct:mapstruct-processor:1.5.2.Final")
}

kapt {
    arguments {
        arg("mapstruct.defaultComponentModel", "spring")
    }
}
```

## documentation

Using kapt: https://kotlinlang.org/docs/reference/kapt.html

MapStruct: http://mapstruct.org/

Kotlin: https://kotlinlang.org/