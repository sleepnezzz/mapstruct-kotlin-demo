package com.sleepy.example.mapper

import com.sleepy.example.dto.request.PersonRequestDto
import com.sleepy.example.model.Person
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test
import org.mapstruct.factory.Mappers

class PersonMapperTests {

    private val mapper = Mappers.getMapper(PersonMapper::class.java)

    private val personRequestDto = PersonRequestDto(0, "firstName", "lastName", "nickName", 10)
    private val person1 = Person(1, "Captal", "Marvel", "Cap", 32)
    private val person2 = Person(2, "John", "Carter", "John", 13)
    private val persons = listOf(person1, person2)

    @Test
    fun toPersonDto() {
        val actual = mapper.toPersonDto(person1)

        assertThat(actual.fname, `is`("Captal"))
        assertThat(actual.lname, `is`("Marvel"))
        assertThat(actual.age, `is`(32))
    }

    @Test
    fun toPersonsResponseDto() {
        val actual = mapper.toPersonsResponseDto(persons)

        assertThat(actual.size, `is`(2))
    }

    @Test
    fun toPerson() {
        val actual = mapper.toPerson(personRequestDto)

        assertThat(actual.id, `is`(0))
        assertThat(actual.firstName, `is`("firstName"))
        assertThat(actual.lastName, `is`("lastName"))
        assertThat(actual.nickName, `is`("nickName"))
        assertThat(actual.age, `is`(10))
    }
}