package com.sleepy.example.mapper

import com.sleepy.example.model.Address
import com.sleepy.example.model.Person
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test
import org.mapstruct.factory.Mappers

class DeliveryAddressMapperTests {

    private val mapper = Mappers.getMapper(DeliveryAddressMapper::class.java)

    private val person = Person(1, "firstName", "lastName", "nickName", 32)

    private val address = Address("street", "12345", "county")

    @Test
    fun toDeliveryAddressDto() {
        val actual = mapper.toDeliveryAddressDto(person, address)
        assertThat(actual.firstName, `is`("firstName"))
        assertThat(actual.lastName, `is`("lastName"))
        assertThat(actual.street, `is`("street"))
        assertThat(actual.postalcode, `is`("12345"))
        assertThat(actual.county, `is`("county"))
    }
}