package com.sleepy.example.controller

import com.sleepy.example.dto.request.PersonRequestDto
import com.sleepy.example.service.PersonService
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class PersonController(private val personService: PersonService) {

    @GetMapping("/persons")
    fun persons() = personService.persons()

    @GetMapping("/person/{id}")
    fun personDetail(@PathVariable id: Int) = personService.detail(id)

    @PostMapping("/person")
    fun personCreate(@RequestBody request: PersonRequestDto) = personService.create(request)

    @DeleteMapping("/person/{id}")
    fun personDelete(@PathVariable id: Int) = personService.delete(id)

    @GetMapping("/delivery")
    fun delivery() = personService.deliveryAddress()
}