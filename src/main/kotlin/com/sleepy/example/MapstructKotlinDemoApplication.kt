package com.sleepy.example

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MapstructKotlinDemoApplication

fun main(args: Array<String>) {
	runApplication<MapstructKotlinDemoApplication>(*args)
}
