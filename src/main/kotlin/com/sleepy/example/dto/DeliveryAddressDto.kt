package com.sleepy.example.dto

data class DeliveryAddressDto(
    var firstName: String?,
    var lastName: String?,
    var street: String,
    var postalcode: String,
    var county: String,
)
