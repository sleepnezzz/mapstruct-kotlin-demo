package com.sleepy.example.dto.request

data class PersonRequestDto(
    val id: Int,
    val firstName: String? = null,
    val lastName: String? = null,
    val nickName: String? = null,
    val age: Int?
)