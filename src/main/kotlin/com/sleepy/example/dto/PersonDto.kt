package com.sleepy.example.dto

data class PersonDto(
    var fname: String? = null,
    var lname: String? = null,
    var age: Int? = null
)
