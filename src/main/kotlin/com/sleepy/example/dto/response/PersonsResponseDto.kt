package com.sleepy.example.dto.response

data class PersonsResponseDto(
    var id: Int = 0,
    var firstName: String? = null,
    var lastName: String? = null,
    var nickName: String? = null,
    var age: Int? = null
)
