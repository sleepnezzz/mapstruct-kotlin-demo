package com.sleepy.example.mapper

import com.sleepy.example.dto.DeliveryAddressDto
import com.sleepy.example.model.Address
import com.sleepy.example.model.Person
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.ReportingPolicy

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface DeliveryAddressMapper {

    @Mapping(source = "person.firstName", target = "firstName")
    @Mapping(source = "person.lastName", target = "lastName")
    @Mapping(source = "address.street", target = "street")
    @Mapping(source = "address.postalcode", target = "postalcode")
    @Mapping(source = "address.county", target = "county")
    fun toDeliveryAddressDto(person: Person, address: Address): DeliveryAddressDto
}