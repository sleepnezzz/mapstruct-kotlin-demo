package com.sleepy.example.mapper

import com.sleepy.example.dto.PersonDto
import com.sleepy.example.dto.request.PersonRequestDto
import com.sleepy.example.dto.response.PersonsResponseDto
import com.sleepy.example.model.Person
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.ReportingPolicy

//@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE) // setting at build.gradle
interface PersonMapper {

    @Mapping(source = "firstName", target = "fname")
    @Mapping(source = "lastName", target = "lname")
    fun toPersonDto(person: Person): PersonDto

    fun toPersonsResponseDto(persons: List<Person>): List<PersonsResponseDto>

    fun toPerson(personRequestDto: PersonRequestDto): Person
}