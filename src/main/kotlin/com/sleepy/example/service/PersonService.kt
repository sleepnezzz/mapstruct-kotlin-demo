package com.sleepy.example.service

import com.sleepy.example.dto.DeliveryAddressDto
import com.sleepy.example.dto.PersonDto
import com.sleepy.example.dto.request.PersonRequestDto
import com.sleepy.example.dto.response.PersonsResponseDto

interface PersonService {
    fun persons(): List<PersonsResponseDto>?
    fun detail(id: Int): PersonDto?
    fun delete(id: Int)
    fun create(request: PersonRequestDto)
    fun deliveryAddress(): DeliveryAddressDto
}