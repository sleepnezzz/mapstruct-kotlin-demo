package com.sleepy.example.service

import com.sleepy.example.dto.DeliveryAddressDto
import com.sleepy.example.dto.PersonDto
import com.sleepy.example.dto.request.PersonRequestDto
import com.sleepy.example.dto.response.PersonsResponseDto
import com.sleepy.example.mapper.DeliveryAddressMapper
import com.sleepy.example.mapper.PersonMapper
import com.sleepy.example.model.Address
import com.sleepy.example.model.Person
import org.mapstruct.factory.Mappers
import org.springframework.stereotype.Service

@Service
class PersonServiceImpl : PersonService {

    private val personMapper = Mappers.getMapper(PersonMapper::class.java)

    private val deliveryAddressMapper = Mappers.getMapper(DeliveryAddressMapper::class.java)

    private val persons = mutableListOf(
        Person(id = 1, firstName = "firstname1", lastName = "lastname1", nickName = "nickname1", age = 15),
        Person(id = 2, firstName = "firstname2", lastName = "lastname2", nickName = "nickname2", age = 16),
        Person(id = 3, firstName = "firstname3", lastName = "lastname3", nickName = "nickname3", age = 18)
    )

    override fun persons(): List<PersonsResponseDto>? = personMapper.toPersonsResponseDto(persons)

    override fun detail(id: Int): PersonDto? =
        persons.find { it.id == id }?.let { personMapper.toPersonDto(it) } ?: throw Exception("person not found")

    override fun delete(id: Int) {
        persons.indexOfFirst { it.id == id }.takeIf { it >= 0 }?.let {
            println("delete person id: $it")
            persons.removeAt(it)
        } ?: throw Exception("person id not found")
    }

    override fun create(request: PersonRequestDto) {
        val person = personMapper.toPerson(request)
        persons.add(person)
    }

    override fun deliveryAddress(): DeliveryAddressDto {
        val person = Person(id = 1, firstName = "firstName", lastName = "lastname")
        val address = Address(street = "street", postalcode = "56110", county = "county")

        return deliveryAddressMapper.toDeliveryAddressDto(person, address)
    }
}