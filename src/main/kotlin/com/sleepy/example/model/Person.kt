package com.sleepy.example.model

data class Person(
    var id: Int = 0,
    var firstName: String? = null,
    var lastName: String? = null,
    var nickName: String? = null,
    var age: Int? = 18
)
