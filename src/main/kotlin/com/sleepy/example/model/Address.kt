package com.sleepy.example.model

data class Address(
    var street: String,
    var postalcode: String,
    var county: String,
)
